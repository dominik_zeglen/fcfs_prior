#include "iostream"
#include "vector"

using namespace std;

class Process
{
	private:
		int arrival;
		int worktime;
		int progress;
		int id;
		int priority;
	
	public:
		int getArrivalTime();
		int getWorkTime();
		int getProgress();
		int getID();
		int getPriority();
		void incPriority();
		Process operator++(int);
		Process(int, int, int, int);
		Process();
		~Process();
};

int Process::getArrivalTime()
{
	return this->arrival;
}

int Process::getWorkTime()
{
	return this->worktime;
}

int Process::getProgress()
{
	return this->progress;
}

int Process::getID()
{
	return this->id;
}

int Process::getPriority()
{
	return this->priority;
}

void Process::incPriority()
{
	this->priority++;
}

Process::Process(int arr, int wrk, int id, int priority)
{
	this->arrival = arr;
	this->worktime = wrk;
	this->progress = 0;
	this->id = id;
	this->priority = priority;
}

Process::Process()
{
	this->arrival = 0;
	this->worktime = 0;
	this->progress = 0;
	this->priority = 0;
}

Process Process::operator++(int)
{
	this->progress++;
	Process temp = *this;
	
	return temp;
}

void swap(Process &a, Process &b)
{
	Process c = a;
	a = b;
	b = c;
}

Process::~Process()
{
	;
}

int main()
{
	int n;
	
	cout << "Prezentacja dzialania priorytetowego algorytmu FCFS z postarzaniem.\nWprowadz liczbe procesow: ";
	cin >> n;
	
	vector<Process> procs(n);
	vector<int> time(n, -1);
	
	for(int i = 0; i < n; i++)
	{
		int a, b, c;
		cout << "\nProces " << i + 1 << "\n\tCzas nadejscia: ";
		cin >> a;
		cout << "\tCzas wykonywania: ";
		cin >> b;
		cout << "\tPriorytet: ";
		cin >> c;
		
		procs[i] = Process(a, b, i, c);
	}
	
	cout << "\n";
	
	for(int t = 0; t < 1000; t++)
	{
		// Szukanie procesu o najnizszym czasie przybycia i najwyzszym priorytecie
		int curr = -1;
		int minArrival = -1;
		int maxPriority = -1;
		
		for(int i = 0; i < n; i++)
		{
			// Przy okazji szukanie procesow do postarzenia
			if((((t - procs[i].getArrivalTime()) % 10) == 0) &&
			((t - procs[i].getArrivalTime()) > 0) &&
			(procs[i].getWorkTime() > procs[i].getProgress()))
			{
				cout << "t = " << t << ", postarzono proces: " << procs[i].getID() << "\n";
				procs[i].incPriority();
			}
				
			if((procs[i].getArrivalTime() <= t) &&
			(procs[i].getWorkTime() > procs[i].getProgress()))
				minArrival = i;
		}
		
		if(minArrival >= 0)
		{
			for(int i = 0; i < n; i++)
			{
				if((procs[i].getArrivalTime() < procs[minArrival].getArrivalTime()) && 
				(procs[i].getArrivalTime() <= t) &&
				(procs[i].getWorkTime() > procs[i].getProgress()))
					minArrival = i;
			}
			
			maxPriority = minArrival;
			
			for(int i = 0; i < n; i++)
			{
				if((procs[i].getPriority() > procs[maxPriority].getPriority()) && 
				(procs[i].getArrivalTime() == procs[minArrival].getArrivalTime()) &&
				(procs[i].getWorkTime() > procs[i].getProgress()))
					maxPriority = i;
			}
		}
		
		curr = maxPriority;
		
		if(curr >= 0)
		{
			// Zapisanie rozpoczecia
			if(time[curr] == -1)
				time[curr] = t;
				
			// Zwiekszenie progressu
			procs[curr]++;
			
			cout << "t = " << t << ", wykonywany proces: " << procs[curr].getID()
			<< "\t\t(" << 1. * procs[curr].getProgress() / procs[curr].getWorkTime() * 100 << "%)"
			<< "\n";
		}
	}
	
	for(int i = 0; i < n; i++)
	{
		cout << "ID: " << procs[i].getID() << ", Czas nadejscia: " << procs[i].getArrivalTime() 
		<< ", Czas wykonywania: " << procs[i].getWorkTime() << ", Wykonanie: "
		<< time[i] << "\n";
	}
	
	return 0;
}
